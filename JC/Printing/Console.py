__author__ = 'vinayjoseph'


def print_two_columns(col1, col2):
    #print '%-30s\t%10s' % (col1, col2)
    print '%16s\t%s' % (col1, col2)

def print_three_columns(col1, col2, col3):
    print '%16s\t%s\t%s' % (col1, col2, col3)

def print_four_columns(col1, col2, col3, col4):
    print '%16s\t%s\t%s\t%s' % (col1, col2, col3, col4)    

def print_seperator_lines():
    print '****************************************'

def print_long_seperator_lines():
    print '**********************************************************************************************************'

def print_very_long_seperator_lines():
    print '***********************************************************************************************************************************************************************'


'''
This function prints big query response in the form of a table.
****************************************
      Query Term                   Count
****************************************
            jobs                      46
      employment                      24
      Employment                      20
donate furniture                      16
feeding the homeless                  16
    Perth stores                      14
             bed                      14
'''


def print_table(big_query_response):
    if 'rows' in big_query_response:
        for row in big_query_response['rows']:
            col1, col2  = (col['v'] for col in row['f'])
            print_two_columns(col1.rjust(20), col2.rjust(16))
    else:
        print_three_columns("Empty".rjust(40),"Empty".rjust(25),"Empty".rjust(25))


def print_three_col_table(big_query_response):
    if 'rows' in big_query_response:
        for row in big_query_response['rows']:
            col1, col2, col3  = (col['v'] for col in row['f'])
            print_three_columns(col1.rjust(40), col2.rjust(25),col3.rjust(25))
    else:
        print_three_columns("Empty".rjust(40),"Empty".rjust(25),"Empty".rjust(25))


def print_four_col_table(big_query_response):
    if 'rows' in big_query_response:
        for row in big_query_response['rows']:
            col1, col2, col3, col4 = (col['v'] for col in row['f'])
            print_four_columns(col1.rjust(50), col2.rjust(5), col3.rjust(5), col4.ljust(100))
    else:
        print_four_columns("Empty".rjust(50),"Empty".rjust(5),"Empty".rjust(5),"Empty".ljust(100))


def print_four_col_table_format_a(big_query_response):

    if 'rows' in big_query_response:
        for row in big_query_response['rows']:
            col1, col2, col3, col4 = (col['v'] for col in row['f'])
            print_four_columns(col1.rjust(60), col2.rjust(5), col3.rjust(5), col4.ljust(5))
    else:
        print_four_columns("Empty".rjust(60),"Empty".rjust(5),"Empty".rjust(5),"Empty".ljust(5))


def print_three_col_table_format_a(big_query_response):
    if 'rows' in big_query_response:
        for row in big_query_response['rows']:
            col1, col2, col3 = (col['v'] for col in row['f'])
            print_three_columns(col1.rjust(65), col2.rjust(1), col3.rjust(1))
    else:
        print_three_columns("Empty".rjust(65), "Empty".rjust(1), "Empty".rjust(1))


def x(big_query_response):

    pass