__author__ = 'vinayjoseph'

import geoip2.webservice
import geoip2.records

import socket

# This creates a client object that can be reused across requests.
# Replace "42" with your user ID and "License_key" with your License
# key

def get_country_iso_code_by_ip(IP):
    # Check if IP is valid
    try:
        socket.inet_aton(IP)
        client = geoip2.webservice.Client(86764,'XU1HHXFH67x1')
        response = client.omni(IP)
        return response.country.iso_code
    except socket.error:
        return "IP not legit"


def get_state_by_ip(IP):
    # Check if IP is valid
    try:
        socket.inet_aton(IP)
        client = geoip2.webservice.Client(86764,'XU1HHXFH67x1')
        response = client.omni(IP)
        return response.subdivisions.most_specific.name
    except socket.error:
        return "IP not legit"


def get_country_name_by_ip(IP):
    #Check if the IP address is valid.
    try:
        socket.inet_aton(IP)
        client = geoip2.webservice.Client(86764,'XU1HHXFH67x1')
        response = client.omni(IP)
        return response.country.name
    except socket.error:
        return "IP not legit"

def get_city_name_by_ip(IP):
    try:
        socket.inet_aton(IP)
        client = geoip2.webservice.Client(86764,'XU1HHXFH67x1')
        response = client.omni(IP)
        print(response.city.name)
    except socket.error:
        return "IP not legit"

def get_postcode_by_ip(IP):
    try:
        socket.inet_aton(IP)
        client = geoip2.webservice.Client(86764,'XU1HHXFH67x1')
        response = client.omni(IP)
        return response.postal.code
    except socket.error:
        return "IP not legit"

#get_city_name_by_ip("124.180.129.133")
get_postcode_by_ip("124.180.129.133")
get_country_name_by_ip("124.180.129.133")
get_country_iso_code_by_ip("124.180.129.133")
get_state_by_ip("124.180.129.133")
