__author__ = 'Vinay Joseph'

import queries
import pdb
import csv

from bigqueryvalues import PROJECT_NUMBER
from JC.BigQuery.queries import TableQuery


def get_special_terms_report():
    path = './JC/BigQuery/special_terms.csv'
    x = csv.DictReader(open(path))
    terms = x.fieldnames
    return terms


class BigQueryWrapper():

    def __init__(self,service,table):
        """

        @type service: Big Query Service
        """
        self._service = service
        self._table = table
        terms = get_special_terms_report()
        self._tableQuery = TableQuery(table, terms)
        
    def get_response(self,queryType):
        svc = self._service

        # Create a query statement and query request object
        query_data = {'query':queryType}
        query_request = svc.jobs()

        # Make a call to the BigQuery API
        query_response = query_request.query(projectId=PROJECT_NUMBER,
                                             body=query_data).execute()
        return query_response

    def get_custom_link_report(self):
        reports = []
        for query in self._tableQuery.special_query_array:
            query_response = self.get_response(query[1])
            row_count = self.get_row_count(query_response)
            reports.append([query[0], row_count])
        return reports

    def get_row_count(self,query_response):
        # Iterate through rows
        for row in query_response['rows']:
            result_row = []
            for field in row['f']:
                result_row.append(field['v'])
                return ('\t').join(result_row)



    def get_total_dyn_nav_rows(self):
        query_response = self.get_response(self._tableQuery.TOTAL_DYN_NAV_COUNT)
        return self.get_row_count(query_response)

    def get_total_rows(self):
        query_response = self.get_response(self._tableQuery.TOTAL_ROW_COUNT)
        return self.get_row_count(query_response)

    def get_total_click_rows(self):
        query_response = self.get_response(self._tableQuery.TOTAL_CLICK_COUNT)
        return self.get_row_count(query_response)

    def get_total_cluster_rows(self):
        query_response = self.get_response(self._tableQuery.TOTAL_CLUSTER_COUNT)
        return self.get_row_count(query_response)

    def get_total_load_rows(self):
        query_response = self.get_response(self._tableQuery.TOTAL_LOAD_COUNT)
        return self.get_row_count(query_response)

    def get_total_keymatch_rows(self):
        query_response = self.get_response(self._tableQuery.TOTAL_KEYMATCH_COUNT)
        return self.get_row_count(query_response)

    def get_total_cluster_rows(self):
        query_response = self.get_response(self._tableQuery.TOTAL_CLUSTER_COUNT)
        return self.get_row_count(query_response)

    def get_total_nextnav_rows(self):
        query_response = self.get_response(self._tableQuery.TOTAL_NEXTNAV_COUNT)
        return self.get_row_count(query_response)

    def get_total_spell_rows(self):
        query_response = self.get_response(self._tableQuery.TOTAL_SPELL_COUNT)
        return self.get_row_count(query_response)

    def get_total_db_rows(self):
        query_response = self.get_response(self._tableQuery.TOTAL_DB_COUNT)
        return self.get_row_count(query_response)

    def get_total_onebox_rows(self):
        query_response = self.get_response(self._tableQuery.TOTAL_ONEBOX_COUNT)
        return self.get_row_count(query_response)

    def get_total_cache_rows(self):
        query_response = self.get_response(self._tableQuery.TOTAL_CACHE_COUNT)
        return self.get_row_count(query_response)

    def get_report_2(self):
        query_response = self.get_response(self._tableQuery.TOTAL_LOADS_WITHOUT_CLICKS)
        return self.get_row_count(query_response)

    def top_20_no_clicks(self):
        query_response = self.get_response(self._tableQuery.TOP_20_QUERIES_WITH_NO_CLICKS)
        return query_response

    def top_10_queries(self):
        query_response = self.get_response(self._tableQuery.TOP_10_Query_terms)
        return query_response

    def get_working_key_matches(self):
        query_response = self.get_response(self._tableQuery.KEYMATCHES_CLICKED)
        return query_response

    def get_clicks_by_domain(self):
        query_response = self.get_response(self._tableQuery.ClICKS_BY_DOMAIN)
        return query_response

    def get_top_users(self):
        query_response = self.get_response(self._tableQuery.TOP_USERS)
        return query_response

    def get_lowest_ranking_queries_with_clicks(self):
        query_response = self.get_response(self._tableQuery.LOWEST_RANKING_QUERIES_AND_PAGES_WITH_CLICKS)
        return query_response

    def get_next_page_report(self):
        query_response = self.get_response(self._tableQuery.NEXT_PAGE_REPORT)
        return  query_response