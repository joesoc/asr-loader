__author__ = 'vinayjoseph'

from JC.BigQuery.bigqueryvalues import PROJECT_NUMBER
from apiclient.errors import HttpError
class TableOperations():

    def __init__(self,service,projectId,datasetId,tableId):
        """

        @param service: BigQuery Service
        @return:
        """
        self._service = service
        self._projectID = projectId
        self._datasetID = datasetId
        self._tableID = tableId

    def delete_table(self):
        svc = self._service
        try:
            svc.tables().delete(projectId=self._projectID,
                                datasetId=self._datasetID,
                                tableId=self._tableID).execute()
        except HttpError as ex:
            print "ERROR:       HTTP Error when deleting table"
