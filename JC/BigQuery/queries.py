__author__ = 'vinayjoseph'


class TableQuery():
    def __init__(self, tablename, terms):
        """
        Takes the name of the BigQuery Table to create
        @param tablename:
        @param terms: array of special terms to query on.
        """
        self._table = tablename
        self.TOTAL_ROW_COUNT = "select count(*) as Total from ASRLogs.%s;" % self._table
        self.TOTAL_DYN_NAV_COUNT = "select count(*) as Total from ASRLogs.%s where Click_Type contains 'dynnav'" % self._table
        self.TOTAL_CLUSTER_COUNT = "select count(*) as Total from ASRLogs.%s where Click_Type contains 'cluster'" % self._table
        self.TOTAL_ADVANCED_COUNT = "select count(*) as Total from ASRLogs.%s where Click_Type contains 'advanced'" % self._table
        self.TOTAL_ADVANCED_SWR_COUNT = "select count(*) as Total from ASRLogs.%s where Click_Type contains 'advanced'" % self._table
        self.TOTAL_CLICK_COUNT = "select count(*) as Total from ASRLogs.%s where Click_Type contains 'c'" % self._table
        self.TOTAL_CACHE_COUNT = "select count(*) as Total from ASRLogs.%s where Click_Type contains 'cache'" % self._table
        self.TOTAL_DB_COUNT = "select count(*) as Total from ASRLogs.%s where Click_Type contains 'db'" % self._table
        self.TOTAL_KEYMATCH_COUNT = "select count(*) as Total from ASRLogs.%s where Click_Type contains 'keymatch'" % self._table
        self.TOTAL_ONEBOX_COUNT = "select count(*) as Total from ASRLogs.%s where Click_Type contains 'onebox'" % self._table
        self.TOTAL_LOAD_COUNT = "select count(*) as Total from ASRLogs.%s where Click_Type contains 'load'" % self._table
        self.TOTAL_NEXTNAV_COUNT = "select count(*) as Total from ASRLogs.%s where Click_Type contains 'nav.next'" % self._table
        self.TOTAL_SPELL_COUNT  = "select count(*) as Total from ASRLogs.%s where Click_Type contains 'spell'" % self._table
        self.TOTAL_SYNONYM_COUNT  = "select count(*) as Total from ASRLogs.%s where Click_Type contains 'synonym'" % self._table
        self.TOTAL_LOADS_WITHOUT_CLICKS = "select count(Query) as total FROM ASRLogs.%s as tbl1 LEFT OUTER JOIN (SELECT IP_Address FROM ASRLogs.%s WHERE Click_Type = 'c') AS tbl2 ON tbl2.IP_Address = tbl1.IP_Address WHERE tbl2.IP_Address IS NULL AND tbl1.Click_Type == 'load';" % (self._table,self._table)
        self.TOP_20_QUERIES_WITH_NO_CLICKS = "SELECT Query, count(Query) as [Count] FROM ASRLogs.%s as tbl1 LEFT OUTER JOIN (SELECT IP_Address FROM ASRLogs.%s WHERE Click_Type = 'c') AS tbl2 ON tbl2.IP_Address = tbl1.IP_Address WHERE tbl2.IP_Address IS NULL AND tbl1.Click_Type == 'load' GROUP BY Query ORDER BY Count DESC LIMIT 20;" % (self._table,self._table)
        self.TOP_10_Query_terms = "select Query, count(Query) as [Total] from ASRLogs.%s group by Query order by Total desc limit 10" % self._table
        self.KEYMATCHES_CLICKED = "select Query, Click_Type as Type, count(Click_Type) as [Count] FROM ASRLogs.%s WHERE Click_Type == 'keymatch' GROUP BY Query, Type ORDER BY Count desc" % self._table
        self.ClICKS_BY_DOMAIN = "select domain(URL) as [Domain],Click_Type as [Type], count(*) as activity " \
                                "from ASRLogs.%s " \
                                "group by [Domain],[Type] " \
                                "having [Domain] is not null and " \
                                "([Type] == 'c' or [Type] == 'keymatch' " \
                                "or [Type] == 'spell' or [Type] == 'dynnav' or [Type] == 'cluster') " \
                                "order by activity desc" % self._table
        self.TOP_USERS = "select IP_ADDRESS as [IP_Address], count(IP_ADDRESS) as [Total] " \
                         "FROM ASRLogs.%s " \
                         "group by [IP_Address] " \
                         "order by [Total] DESC limit 10" % self._table
        self.LOWEST_RANKING_QUERIES_AND_PAGES_WITH_CLICKS = "select Query, count(Query) as [Total], [click_rank] as Rank, URL " \
                                                            "FROM ASRLogs.%s " \
                                                            "where (click_type contains 'c' and integer(click_rank) > 7) " \
                                                            "group by Query, URL, Rank " \
                                                            "order by [Rank] DESC" % self._table
        self.NEXT_PAGE_REPORT = "SELECT Query, Hour(ClickTime) as [Hour], (integer(Click_start)/10) as [Page]" \
                                "FROM [ASRLogs.%s] " \
                                "where Click_Type contains 'nav.next' " \
                                "group by  Query, [Page],[Hour], Query order by Query, [Page] DESC" % self._table


        special_queries = []
        try:
            for term in terms:
                query = "select count(*) as Total from ASRLogs.%s where Click_Type contains '%s'" % (self._table, term)
                special_queries.append([term, query])
        except TypeError:
            pass
            #terms must be empty

        self.special_query_array = special_queries

    @property
    def table(self):
        """Get the value of table"""
        return self._table

    @table.setter
    def table(self,value):
        self._table = value

    @table.deleter
    def table(self):
        del self._table

