__author__ = 'vinayjoseph'

import json
from JC.Printing.Console import print_seperator_lines, print_table, print_two_columns
from JC.BigQuery.asrqueries import BigQueryWrapper

class ETL():
    def __init__(self,http,service,jsonfile,projectId,datasetId,tableId):
        self._http = http
        self._service = service
        self._jsonfile = jsonfile
        self._projectId = projectId
        self._datasetId = datasetId
        self._tableId = tableId

    def load_table(self):

        url = "https://www.googleapis.com/upload/bigquery/v2/projects/integrated-myth-356/jobs"

        schema = open('schema.json','r')

        #create the body of the request, seperated by a boundary of xxx
        newresource = ('--xxx\n' +
                'Content-Type: application/json; charset=UTF-8\n' + '\n' +
                '{\n' +
                '   "configuration": {\n' +
                '     "load": {\n' +
                '       "sourceFormat":"CSV",\n' +
                '       "schema": {\n'
                '         "fields": ' + schema.read() + '\n' +
                '      },\n' +
                '      "destinationTable": {\n' +
                '        "projectId": "' + self._projectId + '",\n' +
                '        "datasetId": "' + self._datasetId + '",\n' +
                '        "tableId": "' + self._tableId + '"\n' +
                '      }\n' +
                '    }\n' +
                '  }\n' +
                '}\n' +
                '--xxx\n' +
                'Content-Type: application/octet-stream\n' +
                '\n')

        show_etl = False
        if show_etl:
            # Signify the end of the body
            print("-------------------------------")
            print("-----New Source before---------")
            print("-------------------------------")
            print(newresource)
            print("-------------------------------")
        # Append data from the specified file to the request body
        f = open(self._jsonfile, 'r')
        newresource += f.read()
        newresource += ('\n')
        newresource += ('--xxx--\n')

        headers = {'Content-Type': 'multipart/related; boundary=xxx'}
        resp, content = self._http.request(url, method="POST", body=newresource, headers=headers)
        print("---------------------------------")
        print("-----Big Query Response obj------")
        print("---------------------------------")
        print("Response status = %s" %resp.status)
        print("Response reason = %s" %resp.reason)
        print("Response fromcache = %s" %resp.fromcache)
        print("Response version = %s" %resp.version)
        print("Response previous = %s" %resp.previous)
        print("---------------------------------")
        print("-----Content obj-----------------")
        print("---------------------------------")
        #print("Content type %s" %(type(content)))
        #print("Content = %s" %content)
        print("---------------------------------")
        if resp.status == 200:
            jsonResponse = json.loads(content)
            jobReference = jsonResponse['jobReference']['jobId']
            import time
            while True:
                jobCollection = self._service.jobs()
                getJob = jobCollection.get(projectId=self._projectId, jobId=jobReference).execute()
                currentStatus = getJob['status']['state']

                if 'DONE' == currentStatus:
                    print "Done Loading!"
                    return
                else:
                  print 'Waiting to load...'
                  print 'Current status: ' + currentStatus
                  print time.ctime()
                  time.sleep(10)


    def push_to_google_bigquery(self):
        print_seperator_lines()
        jsonfile = self.extract_csv()
        print_seperator_lines()
        self.loadTable(self._http, self._service, self._jsonfile)
        print_seperator_lines()
        bqw = BigQueryWrapper(self._service,self._tableId)
        print_seperator_lines()
        return bqw

