# BigQuery and GSA Integration
# Advanced Search Reporting analytics with Big Query

# Objectives

- Extract ASR data from ASR logs
- Clean up ASR data and have it ready for push
- Push clean ASR data to BigQuery
- Run analytics in BigQuery
- Build report for the client.

In the current version report is dumped to the console.

Change the table name before running. 

# Special Term Reporting
If there are special terms, on which reports need to be run, please include them in one line in the file special_terms.csv (comma separated)