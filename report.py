# -*- coding: utf-8 -*-
#
# Copyright (C) 2013 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Command-line skeleton application for BigQuery API.
Usage:
  $ python sample.py

You can also get help on all the command-line flags the program understands
by running:

  $ python sample.py --help

"""

import argparse
import httplib2
import os
import sys
import json

from CleanASR import polish
from JC.BigQuery.TableOps.operations import TableOperations
from apiclient import discovery
from oauth2client import file
from oauth2client import client
from oauth2client import tools
from JC.BigQuery.asrqueries import BigQueryWrapper
from JC.BigQuery.Reports.report import Report
from JC.Printing.Console import print_seperator_lines
from JC.BigQuery.ETL.etl import ETL


'''
-------------------------------------
Change this section to change:
    - Project ID
    - Dataset ID
    - Table
    - Input File
'''
projectId = "integrated-myth-356"
datasetId = "ASRLogs"
tableId = "Salvos_ASR"
TABLE = tableId
INPUT_FILE = "Logs/October_ASR_Salvos.log"
'''
---------------------------------------
'''
# Parser for command-line arguments.
parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawDescriptionHelpFormatter,
    parents=[tools.argparser])

fieldnames = ["ClickTime",
            "IP_Address",
            "Session_ID",
            "Click_Type",
            "Click_start",
            "Click_rank",
            "Click_data",
            "Query",
            "URL"]

# CLIENT_SECRETS is name of a file containing the OAuth 2.0 information for this
# application, including client_id and client_secret. You can see the Client ID
# and Client secret on the APIs page in the Cloud Console:
# <https://cloud.google.com/console#/project/547493192202/apiui>
CLIENT_SECRETS = os.path.join(os.path.dirname(__file__), 'client_secrets.json')

# Set up a Flow object to be used for authentication.
# Add one or more of the following scopes. PLEASE ONLY ADD THE SCOPES YOU
# NEED. For more information on using scopes please see
# <https://developers.google.com/+/best-practices>.
FLOW = client.flow_from_clientsecrets(CLIENT_SECRETS,
  scope=[
      'https://www.googleapis.com/auth/bigquery',
      'https://www.googleapis.com/auth/cloud-platform',
      'https://www.googleapis.com/auth/devstorage.full_control',
      'https://www.googleapis.com/auth/devstorage.read_only',
      'https://www.googleapis.com/auth/devstorage.read_write',
    ],
    message=tools.message_if_missing(CLIENT_SECRETS))


def dump_post(newsource):
    textfile = open('newresource.txt','wb')
    textfile.write(newsource)
    textfile.close()


def extract_csv():
    return 'processedcsv.csv'


def delete_table(tbl_ops):
    print("Deleting Logs")
    print_seperator_lines()
    tbl_ops.delete_table()
    print("Logs deleted")


def etl(http, service):
    print_seperator_lines()
    jsonfile = extract_csv()
    x = ETL(http,service,jsonfile,projectId,datasetId,tableId)
    print_seperator_lines()
    #x.load_table()
    print_seperator_lines()
    bqw = BigQueryWrapper(service,tableId)
    print_seperator_lines()
    return bqw


def spit_report(bqw):
    r = Report(bqw)
    r.generate_reports()


def main(argv):
  # Parse the command-line flags.
  flags = parser.parse_args(argv[1:])

  # If the credentials don't exist or are invalid run through the native client
  # flow. The Storage object will ensure that if successful the good
  # credentials will get written back to the file.
  storage = file.Storage('sample.dat')
  credentials = storage.get()
  if credentials is None or credentials.invalid:
    credentials = tools.run_flow(FLOW, storage, flags)

  # Create an httplib2.Http object to handle our HTTP requests and authorize it
  # with our good Credentials.
  http = httplib2.Http()
  http = credentials.authorize(http)

  # Construct the service object for the interacting with the BigQuery API.
  service = discovery.build('bigquery', 'v2', http=http)

  try:
    tbl_ops = TableOperations(service,projectId=projectId,datasetId=datasetId,tableId=tableId)
    #delete_table(tbl_ops)
    bqw = etl(http, service)
    spit_report(bqw)

  except client.AccessTokenRefreshError:
    print ("The credentials have been revoked or expired, please re-run"
      "the application to re-authorize")


# For more information on the BigQuery API you can visit:
#
#   https://developers.google.com/bigquery/docs/overview
#
# For more information on the BigQuery API Python library surface you
# can visit:
#
#   https://developers.google.com/resources/api-libraries/documentation/bigquery/v2/python/latest/
#
# For information on the Python Client Library visit:
#
#   https://developers.google.com/api-client-library/python/start/get_started
if __name__ == '__main__':
  polish(INPUT_FILE)
  main(sys.argv)
