__author__ = 'vinayjoseph'

import csv

HEADERS = ["ClickTime","IP_Address","Session_ID","Click_Type","Click_start","Click_rank","Click_data","Query","URL"]

def polish(filename):
    file = open(filename)
    reader = csv.DictReader(file,HEADERS)
    with open('processedcsv.csv','wb') as f:
        count = 1
        lines = []
        for row in reader:
            line = ""
            for header in HEADERS:
                if not row[header]:
                    #print("Error : Missing %s in row %s" % (header,count))
                    if header == 'URL':
                        line = line + 'Null' + '\n'
                    else:
                        line = line + 'Null' + ','
                else:
                    if header == 'Query':
                        term = row[header]
                        term = str(term).replace('+',' ')
                        line = line + term + ','
                    elif header == "ClickTime":
                        timevalue = row[header]
                        # for some reason the UNIX time that the GSA gives is wrong. So I need to remove the last two characters.
                        timevalue = timevalue[:-2]
                        line = line + timevalue + ','
                    elif header == 'URL':
                        line = line + row[header] + '\n'
                    else:
                        line = line + row[header] + ','



            f.write(line)
            count = count + 1
